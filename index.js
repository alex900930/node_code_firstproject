const express = require("express");
const dotenv = require("dotenv");

//Configuracion the .env file
dotenv.config();

//Crear nuestra primera APP

const app = express();
const port = process.env.PORT || 8000;

// dEFINIR LA PRIMERA RUTA DE LA Aplicacion(APP)
app.get('/', (req, res)=>{
    res.send(' Bienveniso al api rest full APP Express + TypeScript + Swagger + Mongoose');
});

app.get('/hello', (req, res)=>{
    res.send('Hola Mundo');
});

//Ejecutamos nuestra app y escuchar las peticiones en un puerto especifico
app.listen(port, ()=>{
    console.log(`EXPRESS SERVER: Corriendo en http://localhost:${port}`);
})